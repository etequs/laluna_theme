<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package laluna
 */

?>
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="https://gmpg.org/xfn/11">

	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<?php wp_body_open(); ?>
<div id="page" class="site">

    <div class="header-wrapper">
        <div class="background-wrapper">
            <header id="masthead" class="site-header site-center">

                <div class="logo">
                    <a href="<?php echo get_home_url(); ?>"></a>
                </div>

                <nav id="site-navigation" class="main-navigation">
                    <?php
                    wp_nav_menu(
                        array(
                            'theme_location' => 'main-menu',
                            'menu_id'        => 'main-menu',
                        )
                    );
                    ?>
                </nav><!-- #site-navigation -->
                <div class="toyota-logo"></div>
            </header><!-- #masthead -->
        </div>
        <div class="menu-overlay"></div>
    </div>
    

    <div class="side-menu">
        <?php
        wp_nav_menu(
            array(
                'theme_location' => 'side-menu',
                'menu_id'        => 'side-menu',
            )
        );
        ?>
    </div>
