<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package laluna
 */

get_header();
?>

	<main id="primary" class="site-main">
        <?php
        
        $thumb = get_the_post_thumbnail( $post->ID, 'large' ); 
        $image_fields = get_field('main_image_information');
        


        if ($thumb) : ?>
        <div class="page-main-img">
            <?php echo $thumb; ?>
            <?php 
            if ( $image_fields && $image_fields['enable_block_image_information'] ) : ?>
                <div class="main-image-content">
                    <div class="site-center">
                        <div class="slide-data">
                            <div class="title"><?php echo $image_fields['main_text']; ?></div>
                            <?php get_template_part( 'template-parts/shared/parts/button', null, $image_fields['button'] ); ?>
                            
                        </div>
                    </div>
                </div>
            <?php endif; ?>
        </div>
        <?php endif; ?>
        <div class="page-content">
            <div class="page-title site-center<?php echo $thumb ? '' : ' no-image'; ?>">
                <h1> <?php echo $post->post_title; ?></h1>
            </div>
            
            <?php $blocks = get_field( 'page_blocks' ); ?>
            <?php get_template_part( 'template-parts/page_blocks/page_blocks', null, array( "blocks" => $blocks ) ); ?>

        </div>
	</main><!-- #main -->

<?php
get_footer();
