<?php /* Template Name: Index template */ ?>

<?php 
    $fields = get_fields();
?>

<?php get_header(); ?>

	<main id="primary" class="site-main">

		<?php get_template_part( 'template-parts/index/index-slider', null, array( "fields" => $fields ) ); ?>
		<?php get_template_part( 'template-parts/index/index-cta', null ); ?>
		<?php get_template_part( 'template-parts/shared/car_list', null, array( "fields" => $fields, "title" => "Jauni auto" ) ); ?>
		<?php get_template_part( 'template-parts/shared/news_list', null, array( "fields" => $fields, "title" => "Īpašie piedāvājumi", "posts_per_page" => 3, "post_type" => "special-offer", "more_posts_link" => get_permalink( 147 ) ) ); ?>
		<?php get_template_part( 'template-parts/shared/news_list', null, array( "fields" => $fields, "title" => "Jaunumi", "posts_per_page" => 3, 'show_post_date' => true, "more_posts_link" => get_permalink( 71 ) ) ); ?>

	</main><!-- #main -->

<?php
get_footer();
