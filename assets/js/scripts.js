/* eslint-disable linebreak-style */
/* eslint-disable no-trailing-spaces */
/* eslint-disable padded-blocks */
/* eslint-disable indent */
/* eslint-disable no-console */

/*global jQuery, Swiper, SimpleLightbox */

jQuery( 'document' ).ready( function( $ ) {

    $( '.menu-item-has-children' ).hover( () => {
        $( '.menu-overlay' ).show( 0 );
    }, () => {
        $( '.menu-overlay' ).hide( 0 );
    } );
    
    new Swiper( '.slider', {
        // Optional parameters
        slidesPerView: 1,
        spaceBetween: 20,
        autoplay: {
            delay: 8000,
        },
        
        pagination: {
            el: '.swiper-pagination',
        },
    } );

    new Swiper( '.single-gallery', {
        // Optional parameters
        slidesPerView: 'auto',
        spaceBetween: 0,
        // If we need pagination
    } );

    $( '.single-gallery' ).each( function() {
        const id = $( this ).attr( 'id' );
        new SimpleLightbox( '#' + id + ' .gal-img-wrapper', { sourceAttr: 'data-src' } );
    } );

    $( '.gallery-entry' ).click( function() {

        if ( $( this ).hasClass( 'active' ) ) {
            return;
        } 

        const theGal = $( this ).attr( 'data-gal' );

        console.log( '.single-gallery[data-src="' + theGal + '"]' );

        $( this ).parents( '.gallery-menu' ).find( '.gallery-entry.active' ).removeClass( 'active' );
        $( this ).parents( '.gallery' ).find( '.single-gallery.active' ).removeClass( 'active' );
        $( this ).addClass( 'active' );
        $( this ).parents( '.gallery' ).find( '.single-gallery[data-gal="' + theGal + '"]' ).addClass( 'active' );
    } );

    $( '.vid-caller' ).click( function() {
        const vidSrc = $( this ).attr( 'data-src' );
        const htmlCont = `<div class="vid-wrapper">
            <video src="${ vidSrc }" controls autoplay>
        </div>`;
        $( '.popup-wrapper .popup-main-data' ).html( htmlCont );
        
        $( '.popup-wrapper' ).addClass( 'active' );
    } );

    $( '.popup-wrapper .close-popup' ).click( () => {
        if ( $( '.popup-wrapper video' ).length ) {
            $( '.popup-wrapper video' )[ 0 ].pause();
        }

        $( '.popup-wrapper' ).removeClass( 'active' );
        setTimeout( () => $( '.popup-wrapper .popup-main-data' ).html( '' ), 300 );
    } );

    $( '.term-entry' ).click( function() {

        if ( $( this ).hasClass( 'active' ) ) { 
            return;
        }

        const dataSlug = $( this ).attr( 'data-slug' );

        $( '.term-entry.active' ).removeClass( 'active' );
        $( this ).addClass( 'active' );

        if ( dataSlug === 'all' ) {
            $( '.car-grid-entry-wrapper' ).addClass( 'active' );
        } else {
            $( '.car-grid-entry-wrapper.active' ).removeClass( 'active' );
            $( '.car-grid-entry-wrapper.' + dataSlug ).addClass( 'active' );
            
        }

        resizedw();

        //car-grid-entry-wrapper
    } );

    function setCarListRow() {
       /*  if ( ! $( '.car-list-wrapper.collapsed' ).length ) {
            return;
        } */

        //get item width
        const items = $( '.car-grid-entry-wrapper.active' );
        if ( ! items.length ) {
            return;
        }

        const itemWidth = items[ 0 ].clientWidth - 2;

        const itemsPerRow = Math.round( $( '.car-list' ).width() / itemWidth );
        console.log( itemsPerRow );
        //get items in row

        //get max-height
        let maxHeight = 0;
        
        const forLimit = itemsPerRow <= items.length ? itemsPerRow : items.length;
        
        for ( let i = 0; i < forLimit; i++ ) {
            console.log( i );
            const itemHeight = items[ i ].clientHeight;
            console.dir( items[ i ].clientHeight );

            if ( itemHeight > maxHeight ) {
                maxHeight = itemHeight;
            }
        }

        $( '.car-list-wrapper' ).height( maxHeight );

        if ( itemsPerRow <= items.length ) {
            $( '.button.expand' ).removeClass( 'hidden' );
        } else {
            $( '.button.expand' ).addClass( 'hidden' );
        }
    }

    function fitCarList() {
        const carListWrapper = $( '.car-list-wrapper' );
        carListWrapper.height( $( '.car-list' ).height() );
    }

    $( '.car-list-wrapper .button' ).click( () => {
        if ( $( '.car-list-wrapper' ).hasClass( 'collapsed' ) ) {
            const targetHeight = $( '.car-list' ).height();
            $( '.car-list-wrapper' ).height( targetHeight );
            $( '.car-list-wrapper' ).removeClass( 'collapsed' );
        } else {
            setCarListRow();
            $( '.car-list-wrapper' ).addClass( 'collapsed' );
        }
    } );

    setCarListRow();

    function resizedw() {
        const carListWrapper = $( '.car-list-wrapper' );
        if ( ! carListWrapper ) { 
            return;
        } 
        if ( carListWrapper.hasClass( 'collapsed' ) ) {
            setCarListRow();
        } else {
            fitCarList();
        }
    }
    
    let resizeTimeout;
    window.onresize = function() {
      clearTimeout( resizeTimeout );
      resizeTimeout = setTimeout( resizedw, 100 );
    };

} );
