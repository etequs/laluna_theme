<?php

$terms = get_terms( array(
	'taxonomy'      => 'car_type', // set your taxonomy here
	'hide_empty'    => false, // default: true
) );

$cars = query_posts( array(
    'post_type'         => 'car',
    'post_status'       => 'publish',
    'posts_per_page'    => -1
) );

?>

<div class="car-list-block site-center">
    
    <?php if (isset($args['title'])) : ?>
    <div class="block-header">
        <h2><?php echo $args['title']; ?></h2>
    </div>
    <?php endif; ?>

    <div class="car-type-list">
        <div class="term-entry active" data-slug="all">Visi</div>
        
        <?php foreach ( $terms as $term ) : ?>
    
        <div class="term-entry" data-slug="<?php echo $term->slug; ?>"><?php echo $term->name; ?></div>

        <?php endforeach; ?>
    </div>
    <div class="car-list-wrapper<?php echo isset($args['title']) ? ' collapsed' : ''; ?>">
        <div class="car-list">
            <?php foreach ($cars as $car) : ?>
            
            <?php get_template_part( 'template-parts/shared/parts/car_grid_entry', null, array( "car" => $car ) ); ?>

            <?php endforeach; ?>
        </div>
        <div class="button expand button-with-icon">
            <span class="expand-txt btn-txt">Vairāk</span>
            <span class="collapse-txt btn-txt">Mazāk</span>
            <img class="icon" src="<?php echo get_template_directory_uri(  ); ?>/assets/svg/down-arrow.svg">
        </div>
    </div>
</div>