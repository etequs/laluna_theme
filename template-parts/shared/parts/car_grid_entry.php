<?php
/*
$args = array(
    "car" -> car post objecy
)
*/
if ( !isset($args['car']) ) return;
$car = $args['car'];
$fields = get_fields( $car->ID );
$terms = get_the_terms( $car->ID, 'car_type' );

$car_entry_classes = array();
foreach( $terms as $term ) array_push( $car_entry_classes, $term->slug );

$is_new = ( isset($fields['properties']) && in_array('new', $fields['properties']) );
$is_special = ( isset($fields['properties']) && in_array('special_offer', $fields['properties']) );
$more_props = array();
if ( isset( $fields['properties'] ) ) {
    foreach($fields['properties'] as $prop) {
        if ( $prop == 'new' || $prop == 'special_offer' ) continue;
        if ( $prop == 'hybrid' ) array_push( $more_props, array( "class" => "hybrid", "name" => __("Hibrīds", 'laluna') ) );
        if ( $prop == 'plug_in_hybrid' ) array_push( $more_props, array( "class" => "plug_in_hybrid", "name" => __("Plug-in Hibrīds", 'laluna') ) );
        if ( $prop == 'hybrid_available' ) array_push( $more_props, array( "class" => "hybrid_available", "name" => __("Pieejams Hibrīds", 'laluna') ) );
        if ( $prop == 'hydrogen' ) array_push( $more_props, array( "class" => "hydrogen", "name" => __("Ūdeņradis", 'laluna') ) );
    }
}

?>

<div class="car-grid-entry-wrapper active<?php echo count($car_entry_classes) ? " " . implode( " ", $car_entry_classes ) : ''?>">
    <div class="car-grid-entry">
        <?php echo get_the_post_thumbnail( $car->ID, 'medium', array( 'class' => 'car-grid-img') ); ?>
        <div class="prop-list">
            <?php if ($is_new) { ?> <div class="prop-entry new"><?php _e("Jauns", 'laluna'); ?></div> <?php } ?>
            <?php if ($is_special) { ?> <div class="prop-entry special-offer"><?php _e("Īpašāis piedāvājums", 'laluna'); ?></div> <?php } ?>
            <?php foreach ( $more_props as $prop ) {
                echo sprintf('<div class="prop-entry other %s">%s</div>', $prop['class'], $prop['name']);
            }?>
        </div>
        <div class="car-grid-title"><?php echo $car->post_title; ?></div>
        <div class="price-from">Cena sākot no <?php echo $fields['min_price']?> €</div>
        <a href="<?php echo get_the_permalink( $car->ID ); ?>"></a>
    </div>
</div>