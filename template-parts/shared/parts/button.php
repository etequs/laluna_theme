<?php

    $the_link = '';
    if ( $args['button_target_type'] == 'url' ) {
        $the_link = $args['button_link'];
    } else if ( $args['button_target_type'] == 'post' ) {
        $the_link = get_permalink( $args['button_post_link'] );
    } else if ( $args['button_target_type'] == 'scroll' ) {
        $the_link = '#' . $args['scroll_target_id'];
    } else if ( $args['button_target_type'] == 'file' ) {
        $the_link = $args['file_target'];
    }

?>


<div class="button<?php echo $args['highlight'] ? ' red' : ''; ?> ">
    <span><?php echo $args['button_text']; ?></span>
    <a href="<?php echo $the_link; ?>"></a>
</div>