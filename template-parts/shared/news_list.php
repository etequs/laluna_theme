<?php 


$query_args = array( 
    "post_type"         => isset($args['post_type']) ? $args['post_type'] : 'post',
    "post_status"      => "publish",
    "posts_per_page"    => isset($args['posts_per_page']) ? $args['posts_per_page'] : -1
);

$news = query_posts($query_args);



?>


<div class="posts-block site-center">
    <?php if (isset($args['title'])) : ?>
    <div class="block-header">
        <h2><?php echo $args['title']; ?></h2>
    </div>
    <?php endif; ?>

    <div class="posts-list">
        <?php foreach ($news as $entry) : ?>
        <div class="post-wrapper">
            <div class="post-entry">
                <?php if( isset($args['show_post_date']) && $args['show_post_date'] == true ) : ?>
                    <div class="post-date"><?php echo get_the_date( 'd.m.Y', $entry->ID ); ?></div>
                <?php endif; ?>
                <div class="news-img-wrapper">
                    <?php echo get_the_post_thumbnail( $entry->ID, 'grid-image', array( "class" => "grid-entry-img" ) ); ?>
                </div>
                <h3 class="grid-title"><?php echo $entry->post_title; ?></h3>
                <div class="post-desc"><?php echo get_the_excerpt($entry->ID); ?></div>
                <div class="button-wrapper">
                    <div class="button">
                        <span><?php _e('Apskatīt', 'laluna'); ?></span>
                        <a href="<?php echo get_the_permalink( $entry->ID ); ?>"></a>
                    </div>
                </div>
            </div>
        </div>
        <?php endforeach; ?>
    </div>
    <?php if ( isset( $args['posts_per_page'] ) && isset( $args['more_posts_link'] ) ) : ?>
        <div class="more-button-wrapper">
            <div class="button red">
                <span><?php _e('Apskatīt visus', 'laluna'); ?></span>
                <a href="<?php echo $args['more_posts_link']; ?>"></a>
            </div>
        </div>
    <?php endif; ?>
</div>
