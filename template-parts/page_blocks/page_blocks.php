<?php 
$blocks = get_field( 'page_blocks' );

if ( $blocks ) :
    echo '<div class="page-blocks">';
    foreach ($blocks as $block) {
        echo '<div class="page-block ' . $block['acf_fc_layout'] . '" id=" '. $block['block_id'] .' ">';
        get_template_part( 'template-parts/page_blocks/blocks/block', $block['acf_fc_layout'], array( "block" => $block ) );
        echo '</div>';
    }
    echo '</div>';
endif;