<?php
    $block = $args['block'];  
?>

<div class="block-content-wrapper site-center">
    <div class="contacts-wrapper">
        <div class="col">
            <div class="contacts-data">
                <p class="cont-data-title"><?php echo $block['block_title']; ?></p>
                
                <div class="cont-data-cols">
                    <div class="cont-data-col">
                        <p class="address"><?php echo $block['address']; ?></p>

                        <?php if ( $block['phones'] && count($block['phones']) ) : ?>
                            <div class="phones">
                                <?php foreach( $block['phones'] as $phone_entry) : ?>
                                    <div class="phone"><a href="tel:<?php echo $phone_entry['phone']; ?>"><?php echo $phone_entry['phone']; ?></a></div>
                                <?php endforeach; ?>
                            </div>
                        <?php endif; ?>

                        <?php if ( $block['emails'] && count($block['emails']) ) : ?>
                            <div class="emails">
                                <?php foreach( $block['emails'] as $phone_entry) : ?>
                                    <div class="phone"><a href="mailto:<?php echo $phone_entry['email']; ?>"><?php echo $phone_entry['email']; ?></a></div>
                                <?php endforeach; ?>
                            </div>
                        <?php endif; ?>
                    </div>
                    <div class="cont-data-col">
                        <div class="working-hours">
                            <?php echo $block['working_hours'] ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col">
            <div class="contacts-map">
                <?php echo $block['embed_map'] ?>
            </div>
        </div>
        
        
    </div>
</div>