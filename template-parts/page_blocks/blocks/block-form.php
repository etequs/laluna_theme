<?php
    $block = $args['block'];
?>

<div class="block-content-wrapper site-center">
    <h2 class="block-title"><?php echo $block['block_title']; ?></h2>
    <div class="form-wrapper">
        <?php echo do_shortcode( $block['form_shortcode'] ); ?>
    </div>
</div>