<?php
    $block = $args['block'];
?>
<div class="block-content-wrapper site-center">
    <?php $pos = 'txt-left'; ?>
    <?php foreach( $block['content_blocks'] as $cont ) : ?>
        <?php $pos  = $pos == 'txt-left' ? 'txt-right' : 'txt-left'; ?>
        <div class="concept-entry <?php echo $pos; ?>">
            <div class="img-wrapper">
                <img src="<?php echo $cont['image']['sizes']['large']; ?>">
            </div>
            <div class="concept-content">
                <div class="formatted-text">
                    <?php echo $cont['content']; ?>
                </div>
                <?php $btn = $cont['button']; ?>
                <?php if ( $btn['button_text'] ) : ?>
                    <?php $highlight_class = isset($btn['highlight']) && $btn['highlight'] ? ' red' : ''; ?>
                    <div class="button-wrapper">
                        <div class="button<?php echo $highlight_class; ?>">
                            <span><?php echo $btn['button_text']; ?></span>
                            <a href="<?php echo $btn['button_link']; ?>"></a>
                        </div>
                    </div>
                <?php endif; ?>
            </div>
        </div>
    <?php endforeach; ?>
</div>