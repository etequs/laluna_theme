<?php
    $block = $args['block'];  
?>

<div class="block-content-wrapper site-center">
    <h2 class="block-title"><?php echo $block['block_title']; ?></h2>

    <div class="grid-entries">
        <?php foreach ($block['grid_items'] as $grid_item) : ?>
            <div class="grid-item-wrapper">
                <div class="grid-item">
                    <div class="image-wrapper">
                        <img src="<?php echo $grid_item['image']['sizes']['grid-image']; ?>">
                    </div>
                    <div class="grid-item-desc"><?php echo $grid_item['content']; ?></div>
                </div>
            </div>
        <?php endforeach; ?>
    </div>
</div>