<?php
    $block = $args['block'];
    $video = $block['video'] ? $block['video'] : false;
?>

<div class="block-content-wrapper" style="background-image: url(<?php echo $block['background']['sizes']['large']; ?>)">
    <?php if ($video) : ?>  
        <div class="vid-play-btn vid-caller" id="<?php echo generateRandomString(); ?>" data-src="<?php echo $video['url']?>"></div>
    <?php endif; ?>
    
    <div class="site-center">
        <div class="content">
            <h2><?php echo $block['block_title']; ?></h2>
            <p><?php echo $block['content']; ?></p>
        </div>

    </div>
</div>