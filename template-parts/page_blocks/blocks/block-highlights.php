<?php
    $block = $args['block'];  
?>

<div class="block-content-wrapper site-center">
    <div class="highlight-list">
        <?php foreach ( $block['highlight_entries'] as $highlight_entry ) : ?>
            <div class="highlight-entry">
                <div class="title"><?php echo $highlight_entry['title']; ?></div>
                <div class="content"><?php echo $highlight_entry['content']; ?></div>
            </div>
        <?php endforeach; ?>
    </div>
</div>