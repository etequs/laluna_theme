<?php
    $block = $args['block'];
?>
<div class="block-content-wrapper site-center">
    <?php foreach( $block['button_entries'] as $btn ) : ?>
        <?php $highlight_class = isset($btn['highlight']) && $btn['highlight'] ? ' red' : ''; ?>
        <?php get_template_part( 'template-parts/shared/parts/button', null, $btn ); ?>
    <?php endforeach; ?>
</div>

