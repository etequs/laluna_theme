<?php
    $block = $args['block'];
    
?>

<div class="block-content-wrapper">
    <h2 class="block-title site-center"><?php echo $block['block_title']; ?></h2>
    
    <?php if ( count( $block['gallery_blocks'] ) ) : ?>
        <div class="gallery-menu site-center">
            <?php $index = 0; ?>
            <?php foreach( $block['gallery_blocks'] as $gal ) {
                //var_dump($gal);
                $activeClass = !$index ? ' active' : '';
                echo sprintf( '<div class="gallery-entry%s" data-gal="%s">%s</div>',
                    $activeClass,
                    $gal['gallery_block_title'],
                    $gal['gallery_block_title']
                );
                $index++;
            } ?>
        </div>
    <?php endif; ?>

    <div class="galleries">
        <?php $index = 0; ?>
        <?php foreach( $block['gallery_blocks'] as $gal ) : ?>
            <?php $activeClass = !$index ? ' active' : ''; ?>
            <div class="swiper single-gallery<?php echo $activeClass; ?>" id="<?php echo generateRandomString(); ?>" data-gal="<?php echo $gal['gallery_block_title']; ?>">
                <div class="swiper-wrapper">
                    <?php 
                        foreach( $gal['the_gallery'] as $img ) {
                            echo sprintf('<div class="gal-img-wrapper swiper-slide" data-src="%s"><img src="%s"></div>',
                                $img['sizes']['large'],
                                $img['sizes']['large']
                            );
                        }
                    ?>
                </div>
            </div>
            <?php $index++;?>
        <?php endforeach; ?>
    </div>

</div>


