<?php
    $block = $args['block'];
?>

<div class="block-content-wrapper site-center">
    <div class="image-wrapper">
        <img src="<?php echo $block['image']['sizes']['large'] ?>">
    </div>
    <?php if ( isset($block['caption']) ) : ?>
        <div class="caption"><?php echo $block['caption']; ?></div>
    <?php endif; ?>
</div>