<?php
    $block = $args['block'];  
    $add_class = isset($block['remove_bottom_spacing']) && $block['remove_bottom_spacing'] ? ' no-bottom-spacing' : '';
?>

<div class="block-content-wrapper site-center<?php echo $add_class; ?>">
    <div class="formatted-text">
        <?php echo $block['content']; ?>
    </div>
</div>