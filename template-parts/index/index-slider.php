<?php
   if (!isset($args['fields']['index_slider'])) return;
?>
<div class="slider swiper">
    <div class="slider-wrapper swiper-wrapper">
        <?php foreach( $args['fields']['index_slider'] as $slide ) : ?>
            
            <div class="slide swiper-slide" style="background-image:url(<?php echo $slide['background_image']['url']; ?>)">
                <div class="slide-content site-center">
                    <div class="slide-data">
                        <div class="slide-title"><?php echo $slide['slide_title']; ?></div>
                        <div class="button red">
                            <span><?php echo $slide['button_text']; ?></span>
                            <a href="<?php echo $slide['button_link']; ?>"></a>
                        </div>
                    </div>
                </div>
            </div>
            
        <?php endforeach; ?>
    </div>
    <div class="swiper-pagination"></div>
</div>