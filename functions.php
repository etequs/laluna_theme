<?php


if ( ! defined( '_S_VERSION' ) ) {
	define( '_S_VERSION', '1.0.13' );
}

add_theme_support( 'post-thumbnails' );
add_theme_support( 'customize-selective-refresh-widgets' );
add_image_size( 'grid-image', 510, 285, true );

//disable gutenberg
add_filter('use_block_editor_for_post', '__return_false', 10);
add_action('init', 'my_remove_editor_from_post_type');
function my_remove_editor_from_post_type() {
    remove_post_type_support( 'page', 'editor' );
    remove_post_type_support( 'post', 'editor' );
    add_post_type_support( 'post', 'excerpt' );

}

// Register navigation menus
register_nav_menus( array(
    'main-menu'     => 'main-menu',
    'footer-menu-col-1'   => 'footer-menu-col-1',
    'footer-menu-col-2'   => 'footer-menu-col-2',
    'footer-menu-col-3'   => 'footer-menu-col-3',
    'footer-menu-col-4'   => 'footer-menu-col-4',
    'footer-legal-menu'   => 'footer-legal-menu',
    'side-menu'     => 'side-menu'
));

function laluna_scripts() {
	wp_enqueue_style( 'laluna-style', get_stylesheet_uri(), array(), _S_VERSION );
	wp_enqueue_style( 'swiper-css', 'https://cdn.jsdelivr.net/npm/swiper@8/swiper-bundle.min.css', array(), _S_VERSION );
	wp_enqueue_style( 'lightbox-css', 'https://cdnjs.cloudflare.com/ajax/libs/simplelightbox/2.12.1/simple-lightbox.min.css', array(), _S_VERSION );

    wp_enqueue_script( 'swiper-js', 'https://cdn.jsdelivr.net/npm/swiper@8/swiper-bundle.min.js', array('jquery'), _S_VERSION, true );
    wp_enqueue_script( 'lightbox-js', 'https://cdnjs.cloudflare.com/ajax/libs/simplelightbox/2.12.1/simple-lightbox.min.js', array('jquery'), _S_VERSION, true );
	wp_enqueue_script( 'laluna-main', get_template_directory_uri() . '/assets/js/scripts.js', array('jquery', 'swiper-js', 'lightbox-js'), _S_VERSION, true );

    wp_localize_script( 'main', 'ajaxLocal', array(
		'ajaxUrl' => admin_url( 'admin-ajax.php' )
	));
}
add_action( 'wp_enqueue_scripts', 'laluna_scripts' );


require_once('inc/custom_tax_reg.php');
require_once('inc/custom_post_reg.php');
require_once('inc/side-menu-output.php');
require_once('inc/customizer_options.php');



function generateRandomString($length = 5) {
    $characters = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    $charactersLength = strlen($characters);
    $randomString = '';
    for ($i = 0; $i < $length; $i++) {
        $randomString .= $characters[random_int(0, $charactersLength - 1)];
    }
    return $randomString;
}

