<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package laluna
 */

?>

	<footer id="colophon" class="site-footer">
        <div class="footer-nav-menus site-center">
            <?php
                $menus = array( 'footer-menu-col-1', 'footer-menu-col-2', 'footer-menu-col-3', 'footer-menu-col-4' );

                foreach ($menus as $menu) {
                    $args = array(
                        'theme_location' => $menu,
                        'menu_id'        => $menu,
                    );
                    $term = get_term( get_nav_menu_locations()[$menu], 'nav_menu' );

                    $title = get_field('menu_title', $term);
                    $text_after_menu_items = get_field('text_after_menu_items', $term);

                    echo '<div class="footer-nav-column">';
                    echo '<div class="footer-col-title">' . $title . '</div>';
                    wp_nav_menu( $args );
                    echo '<div class="text-after-menu">' . $text_after_menu_items . '</div>'; 
                    echo '</div>';
                }
            ?>

            
        </div>

        <div class="social-networks site-center">
            <?php
                $theme_mods = get_theme_mods();
            ?>
            <?php if ( $theme_mods['instagram_url'] ) : ?>
                <div class="social-icon instagram">
                    <a href="<?php echo $theme_mods['instagram_url']; ?>"></a>
                </div>
            <?php endif; ?>
            <?php if ( $theme_mods['facebook_url'] ) : ?>
                <div class="social-icon facebook">
                    <a href="<?php echo $theme_mods['facebook_url']; ?>"></a>
                </div>
            <?php endif; ?>
            <?php if ( $theme_mods['youtube_url'] ) : ?>
                <div class="social-icon youtube">
                    <a href="<?php echo $theme_mods['youtube_url']; ?>"></a>
                </div>
            <?php endif; ?>
        </div>

        <div class="legal-information site-center">
            <div class="legal-info-menu">
                <?php
                    wp_nav_menu(
                        array(
                            'theme_location' => 'footer-legal-menu',
                            'menu_id'        => 'footer-legal-menu',
                        )
                    );
                ?>
            </div>
            <div class="copyright">
                <?php _e('© Toyota | SIA "AutoLaluna', 'laluna'); ?>
            </div>
        </div>
	</footer><!-- #colophon -->
</div><!-- #page -->

<div class="popup-wrapper">
    <div class="popup-content">
        <div class="close-popup"></div>
        <div class="popup-main-data">
            
        </div>
    </div>
</div>

<?php wp_footer(); ?>

</body>
</html>
