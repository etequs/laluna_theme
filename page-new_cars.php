<?php /* Template Name: New Cars */ ?>

<?php get_header(); ?>

	<main id="primary" class="site-main">
        <?php $thumb = get_the_post_thumbnail( $post->ID, 'large' ); 
        if ($thumb) : ?>
        <div class="page-main-img">
            <?php echo $thumb; ?>
        </div>
        <?php endif; ?>
        <div class="page-content">
            <div class="page-title site-center<?php echo $thumb ? '' : ' no-image'; ?>">
                <h1> <?php echo $post->post_title; ?></h1>
            </div>
            <?php get_template_part( 'template-parts/shared/car_list', null, array( "fields" => $fields ) ); ?>
        </div>
	</main><!-- #main -->

<?php
get_footer();
