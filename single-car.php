<?php
get_header();
?>

	<main id="primary" class="site-main">
        
        <?php $slide_images = get_field('top_slider_images'); ?>
        <?php $car_fields = get_fields(); ?>

        <div class="slider swiper car-slider">
            <div class="slider-wrapper swiper-wrapper">
                <?php foreach( $slide_images as $slide ) : ?>
                    <div class="slide swiper-slide" style="background-image:url(<?php echo $slide['url']; ?>)"></div>
                <?php endforeach; ?>
            </div>
            <div class="car-slide-content site-center">
                <h1> <?php echo $post->post_title; ?></h1>

                <div class="prop-entry">
                    <div class="prop-title">Sākot no</div>
                    <div class="prop-val"><?php echo $car_fields['min_price']; ?></div>
                </div>
                <div class="prop-entry">
                    <div class="prop-title">Degvielas patēriņš</div>
                    <div class="prop-val">
                        <?php echo $car_fields['consumption']['consumption_from']; ?>
                        -
                        <?php echo $car_fields['consumption']['consumption_to']; ?>
                        <?php _e('l / 100km', 'laluna'); ?>

                    </div>
                </div>
                <div class="prop-entry">
                    <div class="prop-title">CO₂ izmeši</div>
                    <div class="prop-val"><?php echo $car_fields['co2']; ?></div>
                </div>
            </div>
        </div>

        <div class="page-content">
            
            <?php $blocks = get_field( 'page_blocks' ); ?>
            <?php get_template_part( 'template-parts/page_blocks/page_blocks', null, array( "blocks" => $blocks ) ); ?>

        </div>
	</main><!-- #main -->

<?php
get_footer();
