<?php 

add_action( 'init', 'register_theme_body_type_tax' );

function register_theme_body_type_tax() {
    $labels = array(
        'name'              => _x( 'Car types', 'taxonomy general name' ),
        'singular_name'     => _x( 'Car type', 'taxonomy singular name' ),
        'search_items'      => __( 'Search Car types' ),
        'all_items'         => __( 'All Car types' ),
        'parent_item'       => __( 'Parent Car type' ),
        'parent_item_colon' => __( 'Parent Car type:' ),
        'edit_item'         => __( 'Edit Car type' ),
        'update_item'       => __( 'Update Car type' ),
        'add_new_item'      => __( 'Add New Car type' ),
        'new_item_name'     => __( 'New Car type Name' ),
        'menu_name'         => __( 'Car types' ),
    );
    $args   = array(
        'hierarchical'      => true, // make it hierarchical (like categories)
        'labels'            => $labels,
        'show_ui'           => true,
        'show_admin_column' => true,
        'query_var'         => true,
        'rewrite'           => [ 'slug' => 'car_type' ],
    );

    register_taxonomy( 'car_type', [ 'car' ], $args );
}