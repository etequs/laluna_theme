<?php

add_filter('wp_nav_menu_objects', 'my_wp_nav_menu_objects', 10, 2);

function my_wp_nav_menu_objects( $items, $args ) {
    
    // loop
    foreach( $items as &$item ) {
        
        // vars
        $icon = get_field('icon', $item);
        
        
        // append icon
        if( $icon ) {
            
            $item->title = '<div class="hov-title">' . $item->title . '</div><div class="item-icon"><img src="'.$icon['url'].'"></div>';
            
        }
        
    }
    
    
    // return
    return $items;
    
}