<?php
add_action('customize_register', 'theme_footer_customizer');
function theme_footer_customizer($wp_customize){
    //adding section in wordpress customizer   
    $wp_customize->add_section(
        'social_network_links_section',
        array(
            'title'     => 'Social network links'
        ));
    //adding setting for footer text area
    $wp_customize->add_setting('facebook_url', array());

    $wp_customize->add_control(
        'facebook_url',
        array(
            'label'   => 'Facebook link',
            'section' => 'social_network_links_section',
            'type'    => 'text',
        )
    );

    $wp_customize->add_setting('instagram_url', array());

    $wp_customize->add_control(
        'instagram_url',
        array(
            'label'   => 'Instagram link',
            'section' => 'social_network_links_section',
            'type'    => 'text',
        )
    );

    $wp_customize->add_setting('youtube_url', array());

    $wp_customize->add_control(
        'youtube_url',
        array(
            'label'   => 'Youtube link',
            'section' => 'social_network_links_section',
            'type'    => 'text',
        )
    );
}