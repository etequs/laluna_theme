<?php


function register_theme_custom_post_types() {
    register_post_type( 'car',
        array(
            'labels' => array(
                'name' => __( 'Cars' ),
                'singular_name' => __( 'Car' )
            ),
            'public' => true,
            'show_ui' => true,
            'has_archive' => true,
            'taxonomies' => array(''),
            'menu_icon'   => 'dashicons-car',
            'supports' => array('title', 'thumbnail')
        )
    );

    register_post_type( 'special-offer',
        array(
            'labels' => array(
                'name' => __( 'Special offers' ),
                'singular_name' => __( 'Special offer' )
            ),
            'public' => true,
            'show_ui' => true,
            'has_archive' => true,
            'menu_icon'   => 'dashicons-star-filled',
            'supports' => array('title', 'thumbnail', 'excerpt')
        )
    );
}

add_action( 'init', 'register_theme_custom_post_types' );